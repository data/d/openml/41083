# OpenML dataset: Olivetti_Faces

https://www.openml.org/d/41083

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: AT&T Laboratories Cambridge   
**Source**: http://www.cl.cam.ac.uk/research/dtg/attarchive/facedatabase.html  - Date: 1992-1994
**Please cite**:   

This dataset contains a set of face images taken between April 1992 and April 1994 at AT&T Laboratories Cambridge. 

As described on the original website:

There are ten different images of each of 40 distinct subjects. For some subjects, the images were taken at different times, varying the lighting, facial expressions (open / closed eyes, smiling / not smiling) and facial details (glasses / no glasses). All the images were taken against a dark homogeneous background with the subjects in an upright, frontal position (with tolerance for some side movement).
The image is quantized to 256 grey levels and stored as unsigned 8-bit integers; the loader will convert these to floating point values on the interval [0, 1], which are easier to work with for many algorithms.

The “target” for this database is an integer from 0 to 39 indicating the identity of the person pictured; however, with only 10 examples per class, this relatively small dataset is more interesting from an unsupervised or semi-supervised perspective.

The original dataset consisted of 92 x 112, while the version available here consists of 64x64 images.

When using these images, please give credit to AT&T Laboratories Cambridge.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41083) of an [OpenML dataset](https://www.openml.org/d/41083). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41083/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41083/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41083/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

